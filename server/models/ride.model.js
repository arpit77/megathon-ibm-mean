var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var riderData = new Schema({
    "riderName":{
        "type": String
    },
    "numberOfSeats":{
        "type": Number
    }
});

module.exports = mongoose.model('RiderData', riderData);